package com.apps.rio.moviefavorite.db;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.rio.moviefavorite.R;
import com.squareup.picasso.Picasso;

import static com.apps.rio.moviefavorite.db.DatabaseContract.MovieColumns.DESCRIPTION;
import static com.apps.rio.moviefavorite.db.DatabaseContract.MovieColumns.POSTER;
import static com.apps.rio.moviefavorite.db.DatabaseContract.MovieColumns.RELEASE_DATE;
import static com.apps.rio.moviefavorite.db.DatabaseContract.MovieColumns.TITLE;
import static com.apps.rio.moviefavorite.db.DatabaseContract.getColumnString;

/**
 * Created by rio senjou on 19/05/2018.
 */

public class MovieAdapter extends CursorAdapter {


    public MovieAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_movie, viewGroup, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor != null) {
            ImageView poster = (ImageView)view.findViewById(R.id.img_item_photo);
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_card_title);
            TextView tvDate = (TextView) view.findViewById(R.id.tv_release_date);
            TextView tvDescription = (TextView) view.findViewById(R.id.tv_card_overview);




            tvTitle.setText(getColumnString(cursor, TITLE));
            tvDescription.setText(getColumnString(cursor, DESCRIPTION));
            tvDate.setText(getColumnString(cursor,RELEASE_DATE));
            Bitmap setPicture;
            setPicture = getBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(POSTER)));
            poster.setImageBitmap(setPicture);
        }
    }
    public static Bitmap getBitmapFromByte(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    @Override
    public Cursor getCursor() {
        return super.getCursor();
    }
}
