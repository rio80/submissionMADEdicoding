package com.apps.rio.moviefavorite;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.moviefavorite.entity.MovieItm;
import com.squareup.picasso.Picasso;

import static com.apps.rio.moviefavorite.db.DatabaseContract.MovieColumns.POSTER;

public class ActivityDetail extends AppCompatActivity implements View.OnClickListener {
    private MovieItm movieItm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        init();
    }

    public void init(){
        TextView detailJudul,detailReview,detailTanggalRelease;
        Button btnHapusFavorite;
        ImageView detailPoster;
        Cursor cursor = null;

        detailJudul = (TextView)findViewById(R.id.detail_judul);
        detailReview = (TextView)findViewById(R.id.detail_review);
        detailTanggalRelease = (TextView)findViewById(R.id.detail_release_date);
        btnHapusFavorite = (Button) findViewById(R.id.btn_hapus_favorite);
        detailPoster = (ImageView) findViewById(R.id.detail_poster);
        Uri uri = getIntent().getData();
        if (uri != null) {
             cursor = getContentResolver().query(uri, null, null, null, null);

            if (cursor != null){

                if(cursor.moveToFirst()) movieItm = new MovieItm(cursor);

            }
        }

        Bitmap setPicture;
        setPicture = getBitmapFromByte(cursor.getBlob(cursor.getColumnIndex(POSTER)));
        detailPoster.setImageBitmap(setPicture);
        detailJudul.setText(movieItm.getTitle());
        detailTanggalRelease.setText(movieItm.getReleaseDate());
        detailReview.setText(movieItm.getDescription());


        btnHapusFavorite.setOnClickListener(this);
    }

    public static Bitmap getBitmapFromByte(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.btn_hapus_favorite){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Anda yakin ingin hapus favorite ini?");
            dialog.setCancelable(true);
            dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Uri uri = getIntent().getData();
                    getContentResolver().delete(uri, null, null);

                    Toast.makeText(ActivityDetail.this, movieItm.getTitle()+" dihapus dari favorite", Toast.LENGTH_LONG).show();
                    finish();
                }
            });

            dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.create().show();
        }

    }
}
