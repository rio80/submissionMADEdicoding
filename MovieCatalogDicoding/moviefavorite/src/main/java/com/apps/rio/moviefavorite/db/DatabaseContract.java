package com.apps.rio.moviefavorite.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by rio senjou on 19/05/2018.
 */

public class DatabaseContract {
    public static String TABLE_MOVIE = "movie";

    public static final class MovieColumns implements BaseColumns {
        public static String TITLE = "title";
        public static String POSTER = "poster";
        public static String RELEASE_DATE = "release_date";
        public static String DESCRIPTION = "description";

    }

    public static final String AUTHORITY = "com.apps.rio.moviecatalogdicoding";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_MOVIE)
            .build();

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }
    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt( cursor.getColumnIndex(columnName) );
    }
    public static byte[] getColumnBlob(Cursor cursor, String columnName) {
        return cursor.getBlob( cursor.getColumnIndex(columnName) );
    }



}
