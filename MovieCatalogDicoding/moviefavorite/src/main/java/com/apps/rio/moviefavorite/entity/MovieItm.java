package com.apps.rio.moviefavorite.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.apps.rio.moviefavorite.db.DatabaseContract;

import static com.apps.rio.moviefavorite.db.DatabaseContract.getColumnBlob;
import static com.apps.rio.moviefavorite.db.DatabaseContract.getColumnInt;
import static com.apps.rio.moviefavorite.db.DatabaseContract.getColumnString;

/**
 * Created by rio senjou on 19/05/2018.
 */

public class MovieItm implements Parcelable {
    private int id;
    private String title, description, picture,releaseDate;

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.releaseDate);
        parcel.writeString(this.picture);
    }

    public MovieItm(Cursor cursor) {
        this.id = getColumnInt(cursor, DatabaseContract.MovieColumns._ID);
        this.title = getColumnString(cursor, DatabaseContract.MovieColumns.TITLE);
        this.description = getColumnString(cursor, DatabaseContract.MovieColumns.DESCRIPTION);
        this.releaseDate = getColumnString(cursor, DatabaseContract.MovieColumns.RELEASE_DATE);
        this.picture = String.valueOf(getColumnBlob(cursor, DatabaseContract.MovieColumns.POSTER));
    }

    protected MovieItm(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.releaseDate = in.readString();
        this.picture = in.readString();
    }

    public static final Parcelable.Creator<MovieItm> CREATOR = new Parcelable.Creator<MovieItm>() {
        @Override
        public MovieItm createFromParcel(Parcel parcel) {
            return new MovieItm(parcel);
        }

        @Override
        public MovieItm[] newArray(int i) {
            return new MovieItm[i];
        }
    };

}
