package com.apps.rio.moviecatalogdicoding;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.db.DbContract;
import com.apps.rio.moviecatalogdicoding.db.MovieHelper;
import com.apps.rio.moviecatalogdicoding.stackWidget.ImageBannerWidget;
import com.apps.rio.moviecatalogdicoding.stackWidget.StackRemoteViewsFactory;
import com.bumptech.glide.Glide;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static com.apps.rio.moviecatalogdicoding.db.DbContract.AUTHORITY;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.CONTENT_URI;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.BACKDROP;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.DESCRIPTION;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.POSTER;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.RELEASE_DATE;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.TITLE;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.TABLE_MOVIE;

public class posterDetail extends AppCompatActivity implements MaterialFavoriteButton.OnFavoriteChangeListener {
    ImageView posterMovie,backdrop;
    TextView judulMovie;
    TextView deskripsiMovie;
    TextView releaseDate;
    MaterialFavoriteButton favoriteButton;

    private MovieHelper movieHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poster_detail);

        movieHelper = new MovieHelper(this);
        movieHelper.open();

        posterMovie= (ImageView)findViewById(R.id.iv_poster);
        backdrop= (ImageView)findViewById(R.id.iv_backdrop);
        judulMovie = (TextView)findViewById(R.id.tv_detail_judul);
        deskripsiMovie=(TextView)findViewById(R.id.tv_deskripsi_detail);
        releaseDate = (TextView)findViewById(R.id.tv_tgl_release_detail);
        favoriteButton =(MaterialFavoriteButton)findViewById(R.id.favorite_movie);

        favoriteButton.setOnFavoriteChangeListener(this);
        final Intent i =getIntent();
        judulMovie.setText(i.getStringExtra("JUDUL"));

//        setelah load ditambah asBitmap() karena gambar akan di convert dari bitmap ke byte
        Glide.with(getApplicationContext()).load(i.getStringExtra("BACKDROP")).asBitmap().fitCenter().into(backdrop);
        Glide.with(getApplicationContext()).load(i.getStringExtra("POSTER")).asBitmap().fitCenter().into(posterMovie);

        deskripsiMovie.setText(i.getStringExtra("DESKRIPSI"));
        releaseDate.setText(i.getStringExtra("TGLRILIS"));


        posterMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(posterDetail.this,FullPoster.class);
                intent.putExtra("FULLPOSTER",i.getStringExtra("POSTER"));
                startActivity(intent);
            }
        });
    }

    private byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap=((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray=stream.toByteArray();
        return byteArray;
    }

    public static String UPDATE_WIDGET_FAVORITE = "com.apps.rio.moviecatalogdicoding.UPDATE_FAVORITE";

    @Override
    public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {

        ContentValues values = new ContentValues();
        values.put(TITLE,judulMovie.getText().toString());
        values.put(POSTER,imageViewToByte(posterMovie));
        values.put(BACKDROP,imageViewToByte(backdrop));
        values.put(DESCRIPTION,deskripsiMovie.getText().toString());
        values.put(RELEASE_DATE,releaseDate.getText().toString());
        getContentResolver().insert(CONTENT_URI,values);

        onClickRetrieveMovie();


        /*Untuk Update tampilan widget*/
        Intent updateWidgetIntent = new Intent(this, ImageBannerWidget.class);
        updateWidgetIntent.setAction(UPDATE_WIDGET_FAVORITE);
        sendBroadcast(updateWidgetIntent);
        /*-------------------------------------------------*/

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Anda akan dialihkan ke aplikasi favorite, yakin?");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.apps.rio.moviefavorite");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }

            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (movieHelper != null){
            movieHelper.close();
        }
    }

    public void onClickRetrieveMovie() {

        Cursor c = managedQuery(CONTENT_URI, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                Toast.makeText(this,
                        c.getString(c.getColumnIndex(DbContract.MovieColumns._ID)) +
                                ", " + c.getString(c.getColumnIndex(DbContract.MovieColumns.TITLE))
                        ,
                        Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
    }
}
