package com.apps.rio.moviecatalogdicoding.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.model.CustomOnClickListener;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.apps.rio.moviecatalogdicoding.posterDetail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by rio senjou on 05/05/2018.
 */

public class MovieCardAdapter extends RecyclerView.Adapter<MovieCardAdapter.myViewHolder> {
    private Context context;
    private ArrayList<MovieClass> listMovie;
    public void setListMovie(ArrayList<MovieClass> listMovie) {
        this.listMovie = listMovie;
    }

    public ArrayList<MovieClass> getListMovie() {
        return listMovie;
    }

    public MovieCardAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MovieCardAdapter.myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mode, parent, false);
        return new myViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCardAdapter.myViewHolder holder, int position) {
        holder.tvJudul.setText(getListMovie().get(position).getTitle());
        holder.tvOverView.setText(getListMovie().get(position).getOverview());

        Glide.with(context)
                .load(getListMovie().get(position).getPoster_path())
                .override(55, 55)
                .crossFade()
                .into(holder.ivPoster);

        holder.btnDetail.setOnClickListener(new CustomOnClickListener(position, new CustomOnClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                Intent i = new Intent(context,posterDetail.class);
                i.putExtra("JUDUL",listMovie.get(position).getTitle());
                i.putExtra("TGLRILIS",listMovie.get(position).getRelease_date());
                i.putExtra("DESKRIPSI",listMovie.get(position).getOverview());
                i.putExtra("BACKDROP",listMovie.get(position).getBackdrop_path());
                i.putExtra("POSTER",listMovie.get(position).getPoster_path());
                Log.d("POSTER",listMovie.get(position).getPoster_path());
                context.startActivity(i);
            }
        }));

        holder.btnShare.setOnClickListener(new CustomOnClickListener(position, new CustomOnClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                Toast.makeText(context, "test share "+getListMovie().get(position).getTitle(), Toast.LENGTH_SHORT).show();

            }
        }));
    }

    @Override
    public int getItemCount() {
        return getListMovie().size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul,tvOverView;
        ImageView ivPoster;
        Button btnDetail, btnShare;
        public myViewHolder(View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tv_card_title);
            tvOverView = itemView.findViewById(R.id.tv_card_overview);
            ivPoster=itemView.findViewById(R.id.img_item_photo);
            btnDetail = itemView.findViewById(R.id.btn_detail);
            btnShare = itemView.findViewById(R.id.btn_share);
        }
    }
}
