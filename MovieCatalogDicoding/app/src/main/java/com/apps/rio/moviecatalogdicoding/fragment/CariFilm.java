package com.apps.rio.moviecatalogdicoding.fragment;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.apps.rio.moviecatalogdicoding.CatalogueMovieUIUX;
import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.adapter.MovieAdapter;
import com.apps.rio.moviecatalogdicoding.loader.MyAsyncTask;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CariFilm extends Fragment implements AdapterView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<ArrayList<MovieClass>>{
    ListView lvItem;
    MovieAdapter mvAdapter;
    EditText editMovie;
    Button btnCari;
    ArrayList<MovieClass> item = new ArrayList<>();
    static final String EXTRAS_MOVIE = "EXTRAS_MOVIES";
    public CariFilm() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_cari_film, container, false);

        lvItem = (ListView)view.findViewById(R.id.lv_film);
        btnCari = (Button)view.findViewById(R.id.btn_search);
        editMovie = (EditText)view.findViewById(R.id.edt_carifilm);

        btnCari.setOnClickListener(myListener);

        String film = editMovie.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_MOVIE, film);

       getActivity().getSupportLoaderManager().initLoader(0, bundle, this);

        return view;
    }

    @Override
    public Loader<ArrayList<MovieClass>> onCreateLoader(int i, Bundle bundle) {
        String kumpulanMovie = "";
        if (bundle != null) {
            kumpulanMovie = bundle.getString(EXTRAS_MOVIE);
        }

        return new MyAsyncTask(getContext(),kumpulanMovie);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MovieClass>> loader, ArrayList<MovieClass> data ) {
        mvAdapter = new MovieAdapter(getContext());
        mvAdapter.notifyDataSetChanged();

        ((CatalogueMovieUIUX)getActivity()).getSupportActionBar().setTitle(R.string.search_movie);
        lvItem.setAdapter(mvAdapter);
        lvItem.setOnItemClickListener(this);
        mvAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MovieClass>> loader) {

    }

    View.OnClickListener myListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String kota = editMovie.getText().toString();

            if (TextUtils.isEmpty(kota)) return;

            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_MOVIE, kota);

             /*Dengan memanggil restartLoader() maka onCreateLoader akan kembali dijalankan.
            Secara otomatis loader baru akan diciptakan. Cara ini lah yang digunakan untuk mengambil
            data kembali dan kemudian menampilkannya di dalam onLoadFinished.*/
           getActivity().getSupportLoaderManager().restartLoader(0, bundle, CariFilm.this);
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}
