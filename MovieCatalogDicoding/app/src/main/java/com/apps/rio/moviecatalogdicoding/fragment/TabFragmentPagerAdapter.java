package com.apps.rio.moviecatalogdicoding.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by rio senjou on 01/05/2018.
 */

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new NowPlaying();
                break;
            case 1:
                fragment = new UpComing();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
