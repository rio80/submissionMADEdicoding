package com.apps.rio.moviecatalogdicoding;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.apps.rio.moviecatalogdicoding.fragment.CariFilm;
import com.apps.rio.moviecatalogdicoding.fragment.NowPlaying;

import com.apps.rio.moviecatalogdicoding.fragment.UpComing;
import com.apps.rio.moviecatalogdicoding.services.AlarmReceiver;

public class CatalogueMovieUIUX extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    AlarmReceiver alarmReceiver;
    NavigationView navDrawer;
    FragmentTransaction transaction;
    public Fragment fragmentMovie;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue_movie_uiux);

        alarmReceiver = new AlarmReceiver();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.toolbar_menu);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navDrawer = (NavigationView) findViewById(R.id.nav_view);


        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        assert navDrawer != null;
        navDrawer.setNavigationItemSelectedListener(this);

      alarmReceiver.setReminder(CatalogueMovieUIUX.this);
      alarmReceiver.setMovie(CatalogueMovieUIUX.this);

        NowPlaying fragmentNowPlaying = null;
        UpComing fragmentUpComing = null;


        if (savedInstanceState != null) {
            //restore instance dari fragment disini
            fragmentNowPlaying = (NowPlaying)
                    getSupportFragmentManager().findFragmentByTag(SIMPLE_FRAGMENT_TAG);
            fragmentUpComing = (UpComing)
                    getSupportFragmentManager().findFragmentByTag(SIMPLE_FRAGMENT_TAG);
        } else{
            setFragment(new NowPlaying());
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                setFragment(new CariFilm());
                break;

            case R.id.now_playing:
                setFragment(new NowPlaying());
                break;
            case R.id.upcoming:
                setFragment(new UpComing());
                break;
            case R.id.setting_bahasa:
                Intent intent = new Intent(CatalogueMovieUIUX.this, Setting.class);
                startActivity(intent);
                break;
            case R.id.favorite_movie:
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.apps.rio.moviefavorite");
                if (launchIntent != null) {
                    startActivity(launchIntent);
                }
                break;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private final String SIMPLE_FRAGMENT_TAG = "myfragmenttag";

    public void setFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_home, fragment);
            ft.commit();
        }

//        Pemberian TAG SIMPLE_FRAGMENT_TAG berfungsi untuk membuat penanda apakah fragment yang akan dipanggil sesuai
        if (fragment == new NowPlaying()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_home, fragment,SIMPLE_FRAGMENT_TAG);
            ft.commit();

        } if (fragment == new UpComing()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_home, fragment,SIMPLE_FRAGMENT_TAG);
            ft.commit();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
