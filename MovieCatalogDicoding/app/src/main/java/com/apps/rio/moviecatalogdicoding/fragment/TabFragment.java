package com.apps.rio.moviecatalogdicoding.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.CatalogueMovieUIUX;
import com.apps.rio.moviecatalogdicoding.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment {
    ViewPager pager;
    TabLayout tab;
    private String[] pageTitle = {"Now Playing", "Up Comming"};
    public TabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_tab, container, false);
        //inisialisasi tab dan pager
        pager = (ViewPager)view.findViewById(R.id.pager);
        tab = (TabLayout)view.findViewById(R.id.tabs);
        ((CatalogueMovieUIUX)getActivity()).getSupportActionBar().setTitle("List Movie");
        //setting Tab layout (number of Tabs = number of ViewPager pages)

        for (int i = 0; i < pageTitle.length; i++) {
            tab.addTab(tab.newTab().setText(pageTitle[i]));
        }

        //set gravity for tab bar
        tab.setTabGravity(TabLayout.GRAVITY_FILL);


        TabFragmentPagerAdapter pagerAdapter = new TabFragmentPagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);

        //change Tab selection when swipe ViewPager
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));

        tab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.jenis_grid,menu);

        super.onCreateOptionsMenu(menu, inflater);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()){
            case R.id.list_view:
                title = "Mode List";
               *//* Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*//*
                break;
            case R.id.grid_view:
                title = "Mode Grid";
                *//*Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*//*

                break;
            case R.id.card_view:
                title = "Mode Card";
                *//*Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*//*

                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }


}
