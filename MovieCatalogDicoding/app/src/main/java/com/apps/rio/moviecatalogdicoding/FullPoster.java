package com.apps.rio.moviecatalogdicoding;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class FullPoster extends AppCompatActivity {
    ImageView ivFullPoster;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_poster);
        ivFullPoster = (ImageView)findViewById(R.id.iv_full_poster);
        Intent i =getIntent();
        Picasso.get().load(i.getStringExtra("FULLPOSTER")).into(ivFullPoster);
    }
}
