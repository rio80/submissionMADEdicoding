package com.apps.rio.moviecatalogdicoding.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.apps.rio.moviecatalogdicoding.posterDetail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by rio senjou on 05/05/2018.
 */

public class MovieRvAdapter extends RecyclerView.Adapter<MovieRvAdapter.MyViewHolder>{
    private Context context;
    private ArrayList<MovieClass> listMovie = new ArrayList<>();
    public void setListMovie(ArrayList<MovieClass> listMovie) {
        this.listMovie = listMovie;
    }

    public ArrayList<MovieClass> getListMovie() {
        return listMovie;
    }

    public MovieRvAdapter(Context context) {
        this.context = context;
    }
    public void setData(ArrayList<MovieClass> items){
        listMovie=items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MovieRvAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new MovieRvAdapter.MyViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieRvAdapter.MyViewHolder holder, int position) {
        holder.tvJudul.setText(getListMovie().get(position).getTitle());
        holder.tvOverView.setText(getListMovie().get(position).getOverview());
        holder.tvRelease.setText(getListMovie().get(position).getRelease_date());

        Glide.with(context)
                .load(getListMovie().get(position).getPoster_path())
                .override(55, 55)
                .crossFade()
                .into(holder.ivPoster);

    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvJudul,tvOverView,tvRelease;
        ImageView ivPoster;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tv_judul);
            tvOverView = itemView.findViewById(R.id.tv_overview);
            tvRelease = itemView.findViewById(R.id.tv_release_Date);
            ivPoster=itemView.findViewById(R.id.iv_gambar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    Intent i = new Intent(context,posterDetail.class);
                    i.putExtra("JUDUL",listMovie.get(pos).getTitle());
                    i.putExtra("TGLRILIS",listMovie.get(pos).getRelease_date());
                    i.putExtra("DESKRIPSI",listMovie.get(pos).getOverview());
                    i.putExtra("BACKDROP",listMovie.get(pos).getBackdrop_path());
                    i.putExtra("POSTER",listMovie.get(pos).getPoster_path());
                    Log.d("POSTER",listMovie.get(pos).getPoster_path());
                    context.startActivity(i);
                }
            });
        }
    }
}
