package com.apps.rio.moviecatalogdicoding.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.apps.rio.moviecatalogdicoding.posterDetail;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by rio senjou on 05/05/2018.
 */

public class MovieGridAdapter extends RecyclerView.Adapter<MovieGridAdapter.MyViewHolder>{
    private Context context;
    private ArrayList<MovieClass> listMovie;
    public void setListMovie(ArrayList<MovieClass> listMovie) {
        this.listMovie = listMovie;
    }

    public ArrayList<MovieClass> getListMovie() {
        return listMovie;
    }

    public MovieGridAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MovieGridAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_mode, parent, false);
        return new MovieGridAdapter.MyViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieGridAdapter.MyViewHolder holder, int position) {
        Glide.with(context)
                .load(getListMovie().get(position).getBackdrop_path())
                .override(55, 55)
                .crossFade()
                .into(holder.ivBackdrop);
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView ivBackdrop;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivBackdrop = itemView.findViewById(R.id.img_item_photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    Intent i = new Intent(context,posterDetail.class);
                    i.putExtra("JUDUL",listMovie.get(pos).getTitle());
                    i.putExtra("TGLRILIS",listMovie.get(pos).getRelease_date());
                    i.putExtra("DESKRIPSI",listMovie.get(pos).getOverview());
                    i.putExtra("BACKDROP",listMovie.get(pos).getBackdrop_path());
                    i.putExtra("POSTER",listMovie.get(pos).getPoster_path());
                    Log.d("POSTER",listMovie.get(pos).getPoster_path());
                    context.startActivity(i);
                }
            });
        }
    }
}
