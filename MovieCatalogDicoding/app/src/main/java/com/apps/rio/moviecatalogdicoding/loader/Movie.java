package com.apps.rio.moviecatalogdicoding.loader;

/**
 * Created by rio senjou on 02/06/2018.
 */

public interface Movie<V> {
    void onViewAttached(V view);
    void onViewDetached();
    void onDestroyed();
}
