package com.apps.rio.moviecatalogdicoding.stackWidget;

import android.app.Activity;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.db.DbContract;
import com.apps.rio.moviecatalogdicoding.model.MovieItm;

import java.util.ArrayList;
import java.util.List;

import static com.apps.rio.moviecatalogdicoding.db.DbContract.CONTENT_URI;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.BACKDROP;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.POSTER;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.RELEASE_DATE;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.MovieColumns.TITLE;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.getColumnBlob;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.getColumnString;

/**
 * Created by rio senjou on 28/05/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class StackRemoteViewsFactory extends JobService  implements RemoteViewsService.RemoteViewsFactory {
    private MovieItm movieItm;
    private List<MovieItm> mWidgetItems = new ArrayList<>();
    private Context mContext;
    private int mAppWidgetId;
    private Cursor cursor;

    private void initCursor(){
        cursor = mContext.getContentResolver().query(CONTENT_URI,null,null,null,null);

        if (cursor != null){
            if(cursor.moveToFirst()) movieItm = new MovieItm(cursor);
            mWidgetItems.add(movieItm);
        }


    }

    public static Bitmap getBitmapFromByte(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public StackRemoteViewsFactory(Context context, Intent intent) {
        this.mContext = context;
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }
    @Override
    public void onCreate() {
        initCursor();
    }

    @Override
    public void onDataSetChanged() {
        final long identityToken = Binder.clearCallingIdentity();

        initCursor();
        Binder.restoreCallingIdentity(identityToken);

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return cursor.getCount();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        Bitmap setPicture;
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

        cursor.moveToFirst();
        int j = 0;
        while (cursor.getCount() > j){
            cursor.moveToPosition(i);
            setPicture = getBitmapFromByte(getColumnBlob(cursor,BACKDROP));
            rv.setImageViewBitmap(R.id.imageView,setPicture);
            rv.setTextViewText(R.id.item_text,getColumnString(cursor,TITLE));
            rv.setTextViewText(R.id.release_date,getColumnString(cursor,RELEASE_DATE));
            j++;
           /* setPicture = getBitmapFromByte(getBlob(getColumnIndex(POSTER)));
            rv.setImageViewBitmap(R.id.imageView,setPicture);
            rv.setTextViewText(R.id.item_text,getColumnString(cursor, TITLE));*/
        }

        Bundle extras = new Bundle();
        extras.putInt(ImageBannerWidget.EXTRA_ITEM, i);

        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        initCursor();
        jobFinished(jobParameters,false);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
