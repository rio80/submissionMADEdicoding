package com.apps.rio.moviecatalogdicoding.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.apps.rio.moviecatalogdicoding.db.DbContract;

import static com.apps.rio.moviecatalogdicoding.db.DbContract.getColumnBlob;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.getColumnInt;
import static com.apps.rio.moviecatalogdicoding.db.DbContract.getColumnString;


/**
 * Created by rio senjou on 19/05/2018.
 */

public class MovieItm implements Parcelable {
    private int id;
    private String title, description, picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.picture);
    }

    public MovieItm(Cursor cursor) {
        this.id = getColumnInt(cursor, DbContract.MovieColumns._ID);
        this.title = getColumnString(cursor, DbContract.MovieColumns.TITLE);
        this.description = getColumnString(cursor, DbContract.MovieColumns.DESCRIPTION);
        this.picture = String.valueOf(getColumnBlob(cursor, DbContract.MovieColumns.POSTER));
    }

    protected MovieItm(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.picture = in.readString();
    }

    public static final Creator<MovieItm> CREATOR = new Creator<MovieItm>() {
        @Override
        public MovieItm createFromParcel(Parcel parcel) {
            return new MovieItm(parcel);
        }

        @Override
        public MovieItm[] newArray(int i) {
            return new MovieItm[i];
        }
    };

}
