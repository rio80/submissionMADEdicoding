package com.apps.rio.moviecatalogdicoding.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.apps.rio.moviecatalogdicoding.posterDetail;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by rio senjou on 17/04/2018.
 */

public class MovieAdapter extends BaseAdapter {
    private ArrayList<MovieClass> movieClass=new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    String apaaja;

    public MovieAdapter( Context context) {
        this.context = context;
        mInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<MovieClass> items){
        movieClass=items;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (movieClass == null) return 0;
        return movieClass.size();

    }

    @Override
    public Object getItem(int position) {
        return movieClass.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.item,null);
            holder = new ViewHolder();
            holder.ivGambar = convertView.findViewById(R.id.iv_gambar);
            holder.tvJudul =convertView.findViewById(R.id.tv_judul);
            holder.tglRilis = convertView.findViewById(R.id.tv_release_Date);
            holder.deskrispi = convertView.findViewById(R.id.tv_overview);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tvJudul.setText(movieClass.get(position).getTitle());
        holder.tglRilis.setText(movieClass.get(position).getRelease_date());
        holder.deskrispi.setText(movieClass.get(position).getOverview());
        Glide.with(context).load(movieClass.get(position).getPoster_path()).centerCrop().into(holder.ivGambar);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context.getApplicationContext(),movieClass.get(position).getTitle().toString(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context,posterDetail.class);
                i.putExtra("BACKDROP",movieClass.get(position).getBackdrop_path());
                i.putExtra("JUDUL",movieClass.get(position).getTitle());
                i.putExtra("TGLRILIS",movieClass.get(position).getRelease_date());
                i.putExtra("DESKRIPSI",movieClass.get(position).getOverview());
                i.putExtra("POSTER",movieClass.get(position).getPoster_path());

                context.startActivity(i);

            }
        });
        return convertView;
    }

    public class ViewHolder{
        TextView tvJudul;
        ImageView ivGambar;
        TextView tglRilis;
        TextView deskrispi;
    }
}
