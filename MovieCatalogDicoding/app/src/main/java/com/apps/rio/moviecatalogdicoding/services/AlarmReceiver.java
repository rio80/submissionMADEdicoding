package com.apps.rio.moviecatalogdicoding.services;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.CatalogueMovieUIUX;
import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.apps.rio.moviecatalogdicoding.loader.MyAsyncTaskCategory.API_KEY;

/**
 * Created by rio senjou on 31/05/2018.
 */

public class AlarmReceiver extends BroadcastReceiver {
    public Context context;
    String sender, message, url;
    boolean hasil = false;

    private ArrayList<MovieToday> listMovieToday = new ArrayList<>();
    private String[] kategori = {"now_playing", "upcoming"};
    String[] setAlarm;


    public final static String EXTRA_TYPE = "type",
            RELEASE_TODAY = "release_today";

    public final static int NOTIF_GETMOVIE = 101,
            NOTIF_REMINDER = 100;

    private int idNotif = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        String type = intent.getStringExtra(EXTRA_TYPE);

        if (type.equalsIgnoreCase(RELEASE_TODAY)) {
            sender = "Movie hari ini";
            message = "Film Hari ini adalah ";
            getMovie();
        } else {
            sender = "Mengingatkan Kembali";
            message = "Ayo dibuka Aplikasi movie Catalognya";
            sendNotif(sender, message);
        }
        Toast.makeText(context, type, Toast.LENGTH_SHORT).show();
    }



    public void setReminder(Context context) {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, "reminder");
        int requestCode = NOTIF_REMINDER;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        String time;
        time = "07:00";
        setAlarm = time.split(":");
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(setAlarm[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(setAlarm[1]));
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(context, "Repeating alarm set up : " + time, Toast.LENGTH_SHORT).show();
    }

    public void setMovie(Context context) {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, RELEASE_TODAY);
        int requestCode = NOTIF_GETMOVIE;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        String time;
        time = "08:00";
        setAlarm = time.split(":");
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(setAlarm[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(setAlarm[1]));
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(context, "Repeating alarm set up : " + time, Toast.LENGTH_SHORT).show();
    }


    public boolean getMovie() {
        int jmlKategori = kategori.length;
        AsyncHttpClient client1 = new AsyncHttpClient();
        for (int i = 0; i < jmlKategori; i++) {

            url = "https://api.themoviedb.org/3/movie/" + kategori[i] + "?api_key=" + API_KEY + "&language=en-US";

            client1.get(url, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String result = new String(responseBody);
                    try {
                        JSONObject responseJson = new JSONObject(result);
                        JSONArray list = responseJson.getJSONArray("results");

                        int jmlList = list.length();
                        int i = 0;
                        while (i < jmlList) {
                            JSONObject movie = list.getJSONObject(i);
                            MovieClass movieClass = new MovieClass(movie);

                            listMovieToday.add(new MovieToday(
                                    movieClass.getTitle(),
                                    movieClass.getRelease_date()));

                            getMatchTime(movieClass.getTitle(),
                                    movieClass.getRelease_date(),
                                    sender,
                                    message);
                            i++;
                        }

                        hasil = true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hasil = false;
                    }
                }


                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    hasil = false;
                }
            });

        }
        return hasil;
    }


    public void getMatchTime(String judul, String tanggalMovie,String sender,String message) {

        String tglSekarang;
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar1 = Calendar.getInstance();
        tglSekarang = simpleDateFormat1.format(calendar1.getTime());

        if (TextUtils.equals(tanggalMovie, tglSekarang)) {
            message +=judul;
            sendNotif(sender, message);
            idNotif++;
        }
    }



    private void sendNotif(String title, String message) {
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon_movie)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);
        notificationManagerCompat.notify(idNotif, builder.build());

    }

    private class MovieToday {
        private String title;
        private String tglRelease;

        public MovieToday(String title, String tglRelease) {
            this.title = title;
            this.tglRelease = tglRelease;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTglRelease() {
            return tglRelease;
        }

        public void setTglRelease(String tglRelease) {
            this.tglRelease = tglRelease;
        }
    }


}
