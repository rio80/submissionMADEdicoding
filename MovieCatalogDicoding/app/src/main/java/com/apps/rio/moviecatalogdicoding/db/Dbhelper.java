package com.apps.rio.moviecatalogdicoding.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rio senjou on 19/05/2018.
 */

public class Dbhelper extends SQLiteOpenHelper {
    public static String DATABASE_NAME = "dbmovieapps";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_TABLE_MOVIE = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " %s TEXT NOT NULL," +
                    " %s BLOB NOT NULL," +
                    " %s BLOB NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL)",
            DbContract.TABLE_MOVIE,
            DbContract.MovieColumns._ID,
            DbContract.MovieColumns.TITLE,
            DbContract.MovieColumns.POSTER,
            DbContract.MovieColumns.BACKDROP,
            DbContract.MovieColumns.DESCRIPTION,
            DbContract.MovieColumns.RELEASE_DATE);

    public Dbhelper(Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MOVIE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
//        Setelah di drop, buat lagi tabel baru (onCreate(db))
        db.execSQL("DROP TABLE IF EXISTS "+DbContract.TABLE_MOVIE);
        onCreate(db);
    }
}
