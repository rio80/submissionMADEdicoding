package com.apps.rio.moviecatalogdicoding.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.apps.rio.moviecatalogdicoding.CatalogueMovieUIUX;
import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.adapter.MovieCardAdapter;
import com.apps.rio.moviecatalogdicoding.adapter.MovieGridAdapter;
import com.apps.rio.moviecatalogdicoding.adapter.MovieRvAdapter;
import com.apps.rio.moviecatalogdicoding.loader.MyAsyncTaskCategory;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.Toast;


import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpComing extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<MovieClass>> {
    RecyclerView rvUpcoming;
    public static String status="tampil mode";
    ArrayList<MovieClass> dataMovie = new ArrayList<>();
    public final String CATEGORY = "CATEGORY";
    public UpComing() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_up_coming, container, false);
        rvUpcoming = (RecyclerView)view.findViewById(R.id.rv_upcoming);
        ((CatalogueMovieUIUX)getActivity()).getSupportActionBar().setTitle(R.string.menu_upcoming);

        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY,"upcoming");

        if (savedInstanceState != null) {
          /* String hasilInstanteSave ;
           hasilInstanteSave = savedInstanceState.getString("key");
           Toast.makeText(getActivity(), hasilInstanteSave, Toast.LENGTH_SHORT).show();*/

            dataMovie = savedInstanceState.getParcelableArrayList("key");
            showRecyclerList();

        } else {
            getActivity().getSupportLoaderManager().initLoader(0, bundle, this);
        }
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()){
            case R.id.list_view:
                title = "Mode List";
                showRecyclerList();
               /* Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*/
                break;
            case R.id.grid_view:
                title = "Mode Grid";
                /*Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*/
                showRecyclerGrid();
                break;
            case R.id.card_view:
                title = "Mode Card";
                /*Log.d("mode Layout",title);
                Toast.makeText(getContext(), title, Toast.LENGTH_SHORT).show();*/
                showRecyclerCardView();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY, "upcoming");


        if (savedInstanceState != null) {

            dataMovie = savedInstanceState.getParcelableArrayList("key");
            showRecyclerList();

        } else {
            getActivity().getSupportLoaderManager().restartLoader(0, bundle, this);

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Toast.makeText(getContext(), "onSaveInstance", Toast.LENGTH_SHORT).show();

        outState.putParcelableArrayList("key", dataMovie);
        super.onSaveInstanceState(outState);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.jenis_grid,menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void showRecyclerList() {
        rvUpcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        MovieRvAdapter listAdapter = new MovieRvAdapter(getContext());
        listAdapter.setListMovie(dataMovie);
        listAdapter.notifyDataSetChanged();
        rvUpcoming.setAdapter(listAdapter);
    }

    private void showRecyclerGrid() {
        rvUpcoming.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        MovieGridAdapter gridAdapter = new MovieGridAdapter(getActivity());
        gridAdapter.setListMovie(dataMovie);
        gridAdapter.notifyDataSetChanged();
        rvUpcoming.setAdapter(gridAdapter);
    }

    private void showRecyclerCardView(){
        rvUpcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        MovieCardAdapter cardViewAdapter = new MovieCardAdapter(getActivity());
        cardViewAdapter.setListMovie(dataMovie);
        cardViewAdapter.notifyDataSetChanged();
        rvUpcoming.setAdapter(cardViewAdapter);
    }

    @NonNull
    @Override
    public Loader<ArrayList<MovieClass>> onCreateLoader(int id, @Nullable Bundle args) {
        String categori = "";
        if (args != null) {
            categori = args.getString(CATEGORY);
        }

        return new MyAsyncTaskCategory(getContext(),categori);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MovieClass>> loader, ArrayList<MovieClass> data) {
        dataMovie = data;
        showRecyclerList();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MovieClass>> loader) {

    }

    @Override
    public void onStart() {
        super.onStart();

    }
}
