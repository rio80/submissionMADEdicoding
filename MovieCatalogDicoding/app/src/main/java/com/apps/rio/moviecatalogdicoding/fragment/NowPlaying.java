package com.apps.rio.moviecatalogdicoding.fragment;


import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.rio.moviecatalogdicoding.CatalogueMovieUIUX;
import com.apps.rio.moviecatalogdicoding.R;
import com.apps.rio.moviecatalogdicoding.adapter.MovieCardAdapter;
import com.apps.rio.moviecatalogdicoding.adapter.MovieGridAdapter;
import com.apps.rio.moviecatalogdicoding.adapter.MovieRvAdapter;
import com.apps.rio.moviecatalogdicoding.loader.MyAsyncTaskCategory;
import com.apps.rio.moviecatalogdicoding.model.MovieClass;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowPlaying extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<MovieClass>> {
    RecyclerView rvNowplaying;
    ArrayList<MovieClass> dataMovie = new ArrayList<>();
    public final String CATEGORY = "CATEGORY";
    ArrayList<MovieClass> listSaveInstance = new ArrayList<>();
    private MovieClass movieClass;

    public NowPlaying() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_now_playing, container, false);


        rvNowplaying = (RecyclerView) view.findViewById(R.id.rv_nowplaying);
        ((CatalogueMovieUIUX) getActivity()).getSupportActionBar().setTitle(R.string.menu_nowplaying);

        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY, "now_playing");


        if (savedInstanceState != null) {
            dataMovie = savedInstanceState.getParcelableArrayList("key");
            showRecyclerList();

        } else {
            getActivity().getSupportLoaderManager().initLoader(0, bundle, this);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        rvNowplaying = (RecyclerView) view.findViewById(R.id.rv_nowplaying);
        ((CatalogueMovieUIUX) getActivity()).getSupportActionBar().setTitle(R.string.menu_nowplaying);

        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY, "now_playing");

        if (savedInstanceState != null) {
            dataMovie = savedInstanceState.getParcelableArrayList("key");
            showRecyclerList();

        } else {
            getActivity().getSupportLoaderManager().restartLoader(0, bundle, this);

        }
    }

    String tesInstanteSave;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList("key", dataMovie);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.jenis_grid, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()) {
            case R.id.list_view:
                title = "Mode List";
                showRecyclerList();

                break;
            case R.id.grid_view:
                title = "Mode Grid";
                showRecyclerGrid();
                break;
            case R.id.card_view:
                title = "Mode Card";
                showRecyclerCardView();
                break;
        }
        ((CatalogueMovieUIUX)getActivity()).getSupportActionBar().setTitle(title);

        return super.onOptionsItemSelected(item);
    }

    private void showRecyclerList() {
        rvNowplaying.setLayoutManager(new LinearLayoutManager(getActivity()));
        MovieRvAdapter listAdapter = new MovieRvAdapter(getContext());
        listAdapter.setListMovie(dataMovie);
        listAdapter.notifyDataSetChanged();
        rvNowplaying.setAdapter(listAdapter);
    }

    private void showRecyclerGrid() {
        rvNowplaying.setLayoutManager(new GridLayoutManager(getContext(), 2));
        MovieGridAdapter gridAdapter = new MovieGridAdapter(getContext());
        gridAdapter.setListMovie(dataMovie);
        gridAdapter.notifyDataSetChanged();
        rvNowplaying.setAdapter(gridAdapter);
    }

    private void showRecyclerCardView() {
        rvNowplaying.setLayoutManager(new LinearLayoutManager(getContext()));
        MovieCardAdapter cardViewAdapter = new MovieCardAdapter(getContext());
        cardViewAdapter.setListMovie(dataMovie);
        cardViewAdapter.notifyDataSetChanged();
        rvNowplaying.setAdapter(cardViewAdapter);
    }


    @NonNull
    @Override
    public Loader<ArrayList<MovieClass>> onCreateLoader(int id, @Nullable Bundle args) {
        String categori = "";
        if (args != null) {
            categori = args.getString(CATEGORY);
        }

        return new MyAsyncTaskCategory(getContext(), categori);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MovieClass>> loader, ArrayList<MovieClass> data) {
        if (dataMovie.size() <= 0) {
            dataMovie = data;
            showRecyclerList();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("onStart", "start");


    }


    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MovieClass>> loader) {
        dataMovie = null;
    }
}
