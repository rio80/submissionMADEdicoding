package com.apps.rio.moviecatalogdicoding.loader;

import android.content.Context;
import android.os.AsyncTask;

//===============================================
//Gunakan yang atas untuk digunakan pada Activity
//Gunakan yang bawah untuk Fragment

//import android.content.AsyncTaskLoader;
import android.support.v4.content.AsyncTaskLoader;
//===============================================


import android.util.Log;

import com.apps.rio.moviecatalogdicoding.model.MovieClass;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import android.support.v4.content.Loader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by rio senjou on 21/04/2018.
 */

public class MyAsyncTask extends AsyncTaskLoader<ArrayList<MovieClass>> {
    private ArrayList<MovieClass> mData;
    private boolean mHasResult = false;
    private String kumpulanFilm;
    public MyAsyncTask(Context context,String kumpulanFilm) {
        super(context);
        onContentChanged();
        this.kumpulanFilm=kumpulanFilm;
    }

    @Override
    public void deliverResult(ArrayList<MovieClass> data) {
      mData= data;
      mHasResult = true;
      super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()){
            forceLoad();
        }else if (mHasResult){
            deliverResult(mData);
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult){
            onReleaseResources(mData);
            mData = null;
            mHasResult = false;
        }
    }

    private static final String API_KEY= "7117559b48877bf3558dc38bb99c36e3";

    @Override
    public ArrayList<MovieClass> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();
        final ArrayList<MovieClass> movieClasses = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/search/movie?api_key="+API_KEY +
                "&language=en-US&query="+kumpulanFilm;

        Log.d("url",url);

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject responseJson=new JSONObject(result);
                    JSONArray list = responseJson.getJSONArray("results");

                    for (int i=0;i<list.length();i++){
                        JSONObject movie=list.getJSONObject(i);
                        MovieClass movieClass = new MovieClass(movie);
                        movieClasses.add(movieClass);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        return movieClasses;
    }

    protected void onReleaseResources(ArrayList<MovieClass>data){

    }
}
