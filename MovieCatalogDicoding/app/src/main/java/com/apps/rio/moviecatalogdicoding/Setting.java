package com.apps.rio.moviecatalogdicoding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.apps.rio.moviecatalogdicoding.fragment.SettingFragment;

public class Setting extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        getSupportActionBar().setTitle(R.string.toolbar_setting);
        getFragmentManager().beginTransaction().replace(R.id.setting_content
                    ,new SettingFragment()).commit();
    }
}
