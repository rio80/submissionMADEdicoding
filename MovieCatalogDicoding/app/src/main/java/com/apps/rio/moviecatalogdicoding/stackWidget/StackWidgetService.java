package com.apps.rio.moviecatalogdicoding.stackWidget;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.RemoteViewsService;

/**
 * Created by rio senjou on 28/05/2018.
 */

public class StackWidgetService extends RemoteViewsService {
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}
