package com.apps.rio.kamusdicoding.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.apps.rio.kamusdicoding.model.Kamus;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.KAMUS_COLUMNS.ARTI;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.KAMUS_COLUMNS.KATA;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.TABLE_NAME_ENG_IND;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.TABLE_NAME_IND_ENG;

/**
 * Created by rio senjou on 12/05/2018.
 */

public class KamusHelper {
    private Context context;
    private dbHelper dataBaseHelper;

    private SQLiteDatabase database;

    public String tipeBahasa = null;
    private String TABLE_NAME;

    public KamusHelper(Context context) {
        this.context = context;
    }

    public KamusHelper open() throws SQLException {
        dataBaseHelper = new dbHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dataBaseHelper.close();
    }

    public ArrayList<Kamus> getDataByWord(String kata) {
        String result = "";

        if (tipeBahasa == TipeTranslate.ENG_IND) {
            TABLE_NAME = TABLE_NAME_ENG_IND;
        } else if (tipeBahasa == TipeTranslate.IND_ENG) {
            TABLE_NAME = TABLE_NAME_IND_ENG;
        }
        Cursor cursor = database.query(TABLE_NAME, null, KATA + " LIKE ?", new String[]{kata+"%"}, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<Kamus> arrayList = new ArrayList<>();
        Kamus kamusModel;
        if (cursor.getCount() > 0) {
            do {
                kamusModel = new Kamus();
                kamusModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamusModel.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamusModel.setDesk(cursor.getString(cursor.getColumnIndexOrThrow(ARTI)));

                arrayList.add(kamusModel);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<Kamus> getAllData() {
        if (tipeBahasa == TipeTranslate.ENG_IND) {
            TABLE_NAME = TABLE_NAME_ENG_IND;
        } else if (tipeBahasa == TipeTranslate.IND_ENG) {
            TABLE_NAME = TABLE_NAME_IND_ENG;
        }
        String column[] = {KATA};
        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, _ID + " ASC", null);
        cursor.moveToFirst();
        ArrayList<Kamus> arrayList = new ArrayList<>();
        Kamus kamus;
        if (cursor.getCount() > 0) {
            do {
                kamus = new Kamus();
                kamus.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                kamus.setKata(cursor.getString(cursor.getColumnIndexOrThrow(KATA)));
                kamus.setDesk(cursor.getString(cursor.getColumnIndexOrThrow(ARTI)));


                arrayList.add(kamus);
                cursor.moveToNext();


            } while (!cursor.isAfterLast());
        }
        cursor.close();
        Log.d("getAllData",String.valueOf(arrayList.size()));
        return arrayList;
    }

    public long insert(Kamus kamus) {
        if (tipeBahasa == TipeTranslate.ENG_IND) {
            TABLE_NAME = TABLE_NAME_ENG_IND;
        } else if (tipeBahasa == TipeTranslate.IND_ENG) {
            TABLE_NAME = TABLE_NAME_IND_ENG;
        }
        ContentValues initialValues = new ContentValues();
        initialValues.put(KATA, kamus.getKata());
        initialValues.put(ARTI, kamus.getDesk());
        return database.insert(TABLE_NAME, null, initialValues);
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void setTransactionSuccess() {
        database.setTransactionSuccessful();
    }

    public void endTransaction() {
        database.endTransaction();
    }

    public void insertTransaction(Kamus kamus) {
        String sql = "INSERT INTO " + TABLE_NAME + " (" + KATA + ", " + ARTI
                + ") VALUES (?, ?)";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, kamus.getKata());
        stmt.bindString(2, kamus.getDesk());
        stmt.execute();
        stmt.clearBindings();

    }

    public int update(Kamus kamus) {
        ContentValues args = new ContentValues();
        args.put(KATA, kamus.getKata());
        args.put(ARTI, kamus.getDesk());
        return database.update(TABLE_NAME, args, _ID + "= '" + kamus.getId() + "'", null);
    }


    public int delete(int id) {
        return database.delete(TABLE_NAME, _ID + " = '" + id + "'", null);
    }
}
