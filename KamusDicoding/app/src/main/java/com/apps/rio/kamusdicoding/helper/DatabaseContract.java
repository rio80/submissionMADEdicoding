package com.apps.rio.kamusdicoding.helper;

import android.provider.BaseColumns;

/**
 * Created by rio senjou on 12/05/2018.
 */

public class DatabaseContract {
    static String TABLE_NAME_ENG_IND = "table_ENG_IND";
    static String TABLE_NAME_IND_ENG = "table_IND_ENG";

    static final class KAMUS_COLUMNS implements BaseColumns{
        static String KATA = "kata";
        static String ARTI = "arti";
        static String DESK = "desk";

    }
}
