package com.apps.rio.kamusdicoding;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class DetailKamus extends AppCompatActivity {
    TextView tvDetailKata,tvDetailArti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kamus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvDetailKata=(TextView)findViewById(R.id.tv_detail_kata);
        tvDetailArti=(TextView)findViewById(R.id.tv_detail_arti);

        Intent intent=getIntent();
        tvDetailKata.setText(intent.getStringExtra("KATA"));
        tvDetailArti.setText(intent.getStringExtra("ARTI"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if (id==android.R.id.home){
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
