package com.apps.rio.kamusdicoding.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rio.kamusdicoding.DetailKamus;
import com.apps.rio.kamusdicoding.R;
import com.apps.rio.kamusdicoding.model.Kamus;

import java.util.ArrayList;

/**
 * Created by rio senjou on 12/05/2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private ArrayList<Kamus> listKamus = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;


    public ListAdapter(Context context) {
        this.context = context;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public void addItem(ArrayList<Kamus> listData){
        this.listKamus = listData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.MyViewHolder holder, int position) {
        holder.kata.setText(listKamus.get(position).getKata());
        holder.arti.setText(listKamus.get(position).getDesk());
    }

    @Override
    public int getItemCount() {
        return listKamus.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView kata,arti;
        public MyViewHolder(View itemView) {
            super(itemView);
            kata = (TextView)itemView.findViewById(R.id.tv_kata);
            arti = (TextView)itemView.findViewById(R.id.tv_desk);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailKamus.class);
                    intent.putExtra("KATA",kata.getText().toString());
                    intent.putExtra("ARTI",arti.getText().toString());
                    context.startActivity(intent);
                }
            });
        }


    }
}
