package com.apps.rio.kamusdicoding.Fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.rio.kamusdicoding.R;
import com.apps.rio.kamusdicoding.adapter.ListAdapter;
import com.apps.rio.kamusdicoding.helper.KamusHelper;
import com.apps.rio.kamusdicoding.helper.TipeTranslate;
import com.apps.rio.kamusdicoding.model.Kamus;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Ind_Eng extends Fragment {
    RecyclerView rvListKamus;
    ListAdapter adapter;
    ArrayList<Kamus> listKamus;
    KamusHelper kamusHelper;
    SearchView sv;
    public Ind_Eng() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ind_eng, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listKamus = new ArrayList<>();

        rvListKamus = (RecyclerView) view.findViewById(R.id.rv_show_ind_eng);
        rvListKamus.setLayoutManager(new LinearLayoutManager(getActivity()));

        sv=(SearchView)view.findViewById(R.id.sv_ind_eng);

        sv.setQueryHint("Masukan Kata Bahasa Indonesia");
        sv.setFocusable(true);
        kamusHelper = new KamusHelper(getContext());
        adapter = new ListAdapter(getContext());

        rvListKamus.setAdapter(adapter);



        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Kamus> modelKamus=new ArrayList<>();
                modelKamus.clear();

                if (!TextUtils.isEmpty(newText)){
                    kamusHelper.open();
                    kamusHelper.tipeBahasa = TipeTranslate.IND_ENG;
                    modelKamus = kamusHelper.getDataByWord(newText);
                    Log.d("jumlahIndEng", String.valueOf(modelKamus.size()));
                    kamusHelper.close();
                }
                adapter.addItem(modelKamus);

                return true;
            }
        });




    }
}
