package com.apps.rio.kamusdicoding.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.KAMUS_COLUMNS.ARTI;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.KAMUS_COLUMNS.DESK;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.KAMUS_COLUMNS.KATA;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.TABLE_NAME_ENG_IND;
import static com.apps.rio.kamusdicoding.helper.DatabaseContract.TABLE_NAME_IND_ENG;

/**
 * Created by rio senjou on 12/05/2018.
 */

public class dbHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "db_kamus";

    private static final int DATABASE_VERSION = 1;

    public static String CREATE_TABLE_IND_ENG = "CREATE TABLE " + TABLE_NAME_ENG_IND +
            " ( "+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            KATA + " TEXT NOT NULL, "+
            ARTI + " TEXT )";

    public static String CREATE_TABLE_ENG_IND = "CREATE TABLE " + TABLE_NAME_IND_ENG +
            " ( "+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            KATA + " TEXT NOT NULL, "+
            ARTI + " TEXT );";

    public dbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_ENG_IND);
        sqLiteDatabase.execSQL(CREATE_TABLE_IND_ENG);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ CREATE_TABLE_ENG_IND);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ CREATE_TABLE_IND_ENG);
        onCreate(sqLiteDatabase);
    }
}
