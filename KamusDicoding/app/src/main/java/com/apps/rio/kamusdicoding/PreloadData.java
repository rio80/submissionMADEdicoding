package com.apps.rio.kamusdicoding;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.apps.rio.kamusdicoding.helper.AppPreference;
import com.apps.rio.kamusdicoding.helper.KamusHelper;
import com.apps.rio.kamusdicoding.helper.TipeTranslate;
import com.apps.rio.kamusdicoding.model.Kamus;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PreloadData extends AppCompatActivity {
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preload_data);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        new LoadData().execute();
    }

    public class LoadData extends AsyncTask<Void, Integer, Void> {
        final String TAG = LoadData.class.getSimpleName();
        KamusHelper kamusHelperEngInd;
        KamusHelper kamusHelperIndEng;
        AppPreference appPreference;
        double progress;
        double maxprogress = 100;


        @Override
        protected void onPreExecute() {
            kamusHelperEngInd = new KamusHelper(PreloadData.this);
            kamusHelperIndEng = new KamusHelper(PreloadData.this);

            kamusHelperEngInd.tipeBahasa = TipeTranslate.ENG_IND;
            kamusHelperIndEng.tipeBahasa = TipeTranslate.IND_ENG;

            appPreference = new AppPreference(PreloadData.this);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Boolean firstRun = appPreference.getFirstRun();
            if (firstRun) {
                ArrayList<Kamus> listKamusEngInd = preLoadRawEngInd();
                ArrayList<Kamus> listKamusIndEng = preLoadRawIndEng();

                Log.d("PreloadDataEngInd size",String.valueOf(listKamusEngInd.size()));
                Log.d("PreloadDataIndEng size",String.valueOf(listKamusIndEng.size()));


                kamusHelperEngInd.open();
                kamusHelperIndEng.open();

                publishProgress((int) progress);
                Double progressMaxInsert = 80.0;
                Log.d("sizeEngInd", String.valueOf(listKamusEngInd.size()));
                Log.d("sizeIndEng", String.valueOf(listKamusIndEng.size()));

                Double progressDiffEngInd = (progressMaxInsert - progress) / listKamusEngInd.size();
                Double progressDiffIndEng = (progressMaxInsert - progress) / listKamusIndEng.size();

//                Agar bersifat Transactional, Mampu mempercepat proses loading preload
                kamusHelperEngInd.beginTransaction();


                try {
                    for (Kamus model : listKamusEngInd) {
                        kamusHelperEngInd.tipeBahasa = TipeTranslate.ENG_IND;
                        kamusHelperEngInd.insert(model);
                        progress += progressDiffEngInd;
                        publishProgress((int) progress);
                    }
                    kamusHelperEngInd.setTransactionSuccess();
                } catch (Exception e) {
                    Log.e(TAG, "doInBackground: Exception");
                }

                kamusHelperEngInd.endTransaction();
//                =====================================

                kamusHelperEngInd.close();

                kamusHelperIndEng.beginTransaction();

                try {
                    for (Kamus model : listKamusIndEng) {
                        kamusHelperIndEng.tipeBahasa = TipeTranslate.IND_ENG;
                        kamusHelperIndEng.insert(model);
                        progress += progressDiffIndEng;
                        publishProgress((int) progress);
                    }
                    kamusHelperIndEng.setTransactionSuccess();
                } catch (Exception e) {
                    Log.e(TAG, "doInBackground: Exception");
                }

                kamusHelperIndEng.endTransaction();
//                =====================================

                kamusHelperIndEng.close();

                appPreference.setFirstRun(false);

                publishProgress((int) maxprogress);

            } else {
                try {
                    synchronized (this) {
                        this.wait(2000);

                        publishProgress(50);

                        this.wait(2000);
                        publishProgress((int) maxprogress);
                    }
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent i = new Intent(PreloadData.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);
        }
    }


    public ArrayList<Kamus> preLoadRawEngInd() {
        ArrayList<Kamus> kamusModels = new ArrayList<>();
        String line = null;
        BufferedReader reader;
        try {
            Resources res = getResources();
            InputStream raw_dict = res.openRawResource(R.raw.english_indonesia);

            reader = new BufferedReader(new InputStreamReader(raw_dict));
            int count = 0;
            do {
                line = reader.readLine();
                String[] splitstr = line.split("\t");

                Kamus kamusModel;

                kamusModel = new Kamus(splitstr[0], splitstr[1]);
                kamusModels.add(kamusModel);
                count++;
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("PreloadData size",String.valueOf(kamusModels.size()));
        return kamusModels;
    }


    public ArrayList<Kamus> preLoadRawIndEng() {
        ArrayList<Kamus> kamusModels = new ArrayList<>();
        String line = null;
        BufferedReader reader;
        try {
            Resources res = getResources();
            InputStream raw_dict = res.openRawResource(R.raw.indonesia_english);

            reader = new BufferedReader(new InputStreamReader(raw_dict));
            int count = 0;
            do {
                line = reader.readLine();
                String[] splitstr = line.split("\t");

                Kamus kamusModel;

                kamusModel = new Kamus(splitstr[0], splitstr[1]);
                kamusModels.add(kamusModel);
                count++;
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("PreloadData size",String.valueOf(kamusModels.size()));
        return kamusModels;
    }
}
