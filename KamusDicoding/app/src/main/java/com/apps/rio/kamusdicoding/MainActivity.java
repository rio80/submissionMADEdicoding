package com.apps.rio.kamusdicoding;

import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;

import com.apps.rio.kamusdicoding.Fragment.Eng_Ind;
import com.apps.rio.kamusdicoding.Fragment.Ind_Eng;
import com.apps.rio.kamusdicoding.Fragment.WelcomeScreen;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    NavigationView navView;
    ActionBarDrawerToggle actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navView = (NavigationView) findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Aplikasi Kamus Dicoding");
        getSupportActionBar().setSubtitle("Indonesia to English");

        actionBar = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBar);
        actionBar.syncState();

        navView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            Fragment currentFragment;
            currentFragment = new Ind_Eng();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, currentFragment).commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        actionBar = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBar);
        actionBar.syncState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        drawerLayout.removeDrawerListener(actionBar);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        String title = "";
        int id;
        id = item.getItemId();

        if (id == R.id.eng_ind) {
            title = "English to Indonesia";
            fragment = new Eng_Ind();
        }

        if (id == R.id.ind_eng) {
            title = "Indonesia to English";
            fragment = new Ind_Eng();
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
        }

        getSupportActionBar().setSubtitle(title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
