package com.apps.rio.kamusdicoding.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rio senjou on 12/05/2018.
 */

public class Kamus implements Parcelable {
    private int id;
    private String kata;
    private String desk;


    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }


    public Kamus() {
    }

    protected Kamus(Parcel in) {

    }

    public Kamus(String kata, String desk) {
        this.kata = kata;
        this.desk = desk;
    }

    public static final Creator<Kamus> CREATOR = new Creator<Kamus>() {
        @Override
        public Kamus createFromParcel(Parcel in) {
            return new Kamus(in);
        }

        @Override
        public Kamus[] newArray(int size) {
            return new Kamus[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKata() {
        return kata;
    }

    public void setKata(String kata) {
        this.kata = kata;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {


    }
}
